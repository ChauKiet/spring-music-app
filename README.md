# Spring Music App

Used Spring Initializr to create a web app with Thymeleaf that reads from the Chinook database and exposes REST endpoints for potential future mobile apps.

- Two views created in Thymeleaf:
    - Homepage: includes a searchbar and three lists of five random artists, five random songs, and five random playlists.
    - Searchpage: Shows the first song that fits the search criteria, the search will look for any songs that includes the search term in any way.

- API endpoint functionality:
    - Reads all customers information.
    - Adds a new customer.
    - Updates existing customer.
    - Returns the number of costumers in each country.
    - Highest spending customers.
    - A specific customers most specific genre.
