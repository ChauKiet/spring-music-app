package Music.controllers;


import Music.data_access.CustomerRepository;
import Music.models.CountryCount;
import Music.models.Customer;
import Music.models.CustomerTotal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class TrackController {
    CustomerRepository customerRepository = new CustomerRepository();

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value = "/customers/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable String id) {
        return customerRepository.getCustomerById(id);
    }

    @RequestMapping(value = "/customers/contries", method = RequestMethod.GET)
    public ArrayList<CountryCount> getCountryCount() {
        return customerRepository.getCountryCount();
    }

    @RequestMapping(value = "/customers/spenders", method = RequestMethod.GET)
    public ArrayList<CustomerTotal> getHighestSpenders() {
        return customerRepository.getHighestSpenders();
    }

}
