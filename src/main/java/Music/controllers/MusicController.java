package Music.controllers;


import Music.models.Track;
import org.springframework.stereotype.Controller;
import Music.data_access.SongRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;


@Controller
public class MusicController {
    SongRepository songRepository = new SongRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("artists", songRepository.getRanArtists());
        model.addAttribute("songs", songRepository.getRanSongs());
        model.addAttribute("playlists", songRepository.getRanPlaylists());
        model.addAttribute("error", "");
        return "home";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String simpleSearch(@RequestParam("query") String searched, Model model) {
        Track song = songRepository.getSong(searched);
        if (song == null) {
            model.addAttribute("artists", songRepository.getRanArtists());
            model.addAttribute("songs", songRepository.getRanSongs());
            model.addAttribute("playlists", songRepository.getRanPlaylists());
            model.addAttribute("error", "That songtitle does not exist in our database.");
            return "home";
            //return "error";
        } else {
            model.addAttribute("searched", searched);
            model.addAttribute("song", song);
            model.addAttribute("genre", song.getGenre());
            model.addAttribute("artist", song.getArtist());
            model.addAttribute("album", song.getAlbum());
            model.addAttribute("title", song.getTitle());
            return "search";
        }
    }

}
