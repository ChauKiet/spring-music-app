package Music.models;

public class CustomerTotal {
    private String name;
    private int total;

    public CustomerTotal(String name, int total) {
        this.name = name; this.total = total;
    }

    public String getName() {
        return name;
    }

    public int getTotal() {
        return total;
    }
}
