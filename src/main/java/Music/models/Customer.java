package Music.models;

public class Customer {
    private int customerId;
    private String firstName;
    private String lastName;
    private String country;
    private int postalCode;
    private int phone;

    public Customer(int customerId, String firstName, String lastName, String country, int postalCode, int phone) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCountry() {
        return country;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public int getPhone() {
        return phone;
    }
}
