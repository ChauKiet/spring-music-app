package Music.data_access;

import java.sql.*;
import java.util.ArrayList;

import Music.models.CountryCount;
import Music.models.Customer;
import Music.models.CustomerTotal;
import Music.models.Track;

public class CustomerRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "Select CustomerId, FirstName, LastName, Country, PostalCode, Phone " +
                                    "FROM customer");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getInt("PostalCode"),
                        resultSet.getInt("Phone")
                ));
            }
            System.out.println("All customers gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return customers;
    }

    public Customer getCustomerById(String id) {
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "Select CustomerId, FirstName, LastName, Country, PostalCode, Phone " +
                                    "FROM customer WHERE CustomerId = ?");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            customer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getInt("PostalCode"),
                    resultSet.getInt("Phone")
            );
            System.out.println("Customer gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return customer;
    }

    public ArrayList<CountryCount> getCountryCount() {
        ArrayList<CountryCount> countryCounts = new ArrayList<CountryCount>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "SELECT Country, COUNT(*) as Count FROM customer GROUP BY Country ORDER BY Count DESC");
            /*
            System.out.println("before");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Country, count(*) as Count FROM Customer GROUP BY Country ORDER BY Count DESC");
            */System.out.println("horrific");

            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println("horror");
            while (resultSet.next()) {
                countryCounts.add(new CountryCount(
                        resultSet.getString("Country"),
                        resultSet.getInt("Count")
                        ));
            }
            System.out.println("Country counts gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return countryCounts;
    }

    public ArrayList<CustomerTotal> getHighestSpenders() {
        ArrayList<CustomerTotal> highestSpenders = new ArrayList<CustomerTotal>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "SELECT CONCAT(FirstName, ' ' , LastName) as Name, Total FROM customer " +
                                    "INNER JOIN invoice ON customer.CustomerId = invoice.CustomerId " +
                                    "GROUP BY Total ORDER BY Total DESC");
            System.out.println("horrific");
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println("horror");
            while (resultSet.next()) {
                highestSpenders.add(new CustomerTotal(
                        resultSet.getString("Name"),
                        resultSet.getInt("Total")
                ));
            }
            System.out.println("Highest spenders gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return highestSpenders;
    }
}
